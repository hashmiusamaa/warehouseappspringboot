    selectedStorage = {};
function finalize(){
    alert("Finalize called.");
    $.ajax({
        url: "/item/add",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(selectedItem),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                selectedStorage.itemID = parseInt(JSON.stringify(response));
                storeStorage();
                alert(JSON.stringify(response));
                getAllItems();
            }
            setWrapperHeight();
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
            setWrapperHeight();
        }
    });
}

function storeStorage(){
    selectedStorage.id = $('#storage_ID').val();
    selectedStorage.shelfNumber = $('#storage_shelfNumber').val();
    selectedStorage.rackNumber = $('#storage_rackNumber').val();
    selectedStorage.dateOfEntry = $('#storage_dateOfEntry').val();
    selectedStorage.expectedDateOfExit = $('#storage_expectedDateOfExit').val();
    selectedStorage.dateOfExit = $('#storage_dateOfExit').val();
    selectedStorage.amountCharged = $('#storage_shelfNumber').val();
    selectedStorage.status = $('#storage_status').val();
    $.ajax({
        url: "/storage/add",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(selectedStorage),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                alert(JSON.stringify(response));
                getAllStorages();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
            setWrapperHeight();
        }
    });
}


globalstorages = [];
selectedStorage = {};
function getAllStorages(){

    $.ajax({
        url: "/storage/list",
        type: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                listStorages(response);
            }
            setWrapperHeight();
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
            setWrapperHeight();
        }
    });
}

function listStorages(storages){
    $('#storages').text('');
    globalstorages = storages;
    for(storage in storages){
        node = "<tr>";
        node += "<td>"
        node += storages[storage].id;
        node += "</td>"
        node += "<td>"
        node += storages[storage].itemID;
        node += "</td>"
        node += "<td>"
        node += storages[storage].shelfNumber;
        node += "</td>"
        node += "<td>"
        node += storages[storage].rackNumber;
        node += "</td>"
        node += "<td>"
        node += storages[storage].dateOfEntry;
        node += "</td>"
        node += "<td>"
        node += storages[storage].expectedDateOfExit;
        node += "</td>"
        node += "<td>"
        node += storages[storage].dateOfExit;
        node += "</td>"
        node += "<td>"
        node += storages[storage].amountCharged;
        node += "</td>"
        node += "<td>"
        node += storages[storage].status;
        node += "</td>"
        node += "<td><span " +
            "onclick=\"loadStorage("+storage+")\" " +
            ">load</span>"
        node += "</td>"
        node += "</tr>"
        $('#storages').append(node);
    }
}

function loadStorage(storage){
    storageToLoad = globalstorages[storage];
    clear();
    selectedStorage = storageToLoad;
    alert(JSON.stringify(storageToLoad));
    $('#storage_ID').val(storageToLoad.id);
    $('#storage_itemID').val(storageToLoad.itemID);
    $('#storage_shelfNumber').val(storageToLoad.shelfNumber);
    $('#storage_rackNumber').val(storageToLoad.rackNumber);
    $('#storage_dateOfEntry').val(storageToLoad.dateOfEntry);
    $('#storage_expectedDateOfExit').val(storageToLoad.expectedDateOfExit);
    $('#storage_dateOfExit').val(storageToLoad.dateOfExit);
    $('#storage_amountCharged').val(storageToLoad.amountCharged);
    $('#storage_status').val(storageToLoad.status);
    selectedItem.storageID = storageToLoad.id;
}

function addStorage() {
    var storage = {};
    storage.id = $('#storage_ID').val();
    storage.itemID = -1;
    storage.shelfNumber = $('#storage_shelfNumber').val();
    storage.rackNumber = $('#storage_rackNumber').val();
    storage.dateOfEntry = $('#storage_dateOfEntry').val();
    storage.expectedDateOfExit = $('#storage_expectedDateOfExit').val();
    storage.dateOfExit = $('#storage_dateOfExit').val();
    storage.amountCharged = $('#storage_shelfNumber').val();
    storage.status = $('#storage_status').val();
    //selectedStorage = storage;
    $.ajax({
        url: "/storage/add",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(storage),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                alert(JSON.stringify(response));
                getAllStorages();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
        }
    });
}

function clear(){
    $('#storage_ID').val('');
    $('#storage_itemID').val('');
    $('#storage_shelfNumber').val('');
    $('#storage_rackNumber').val('');
    $('#storage_dateOfEntry').val('');
    $('#storage_expectedDateOfExit').val('');
    $('#storage_dateOfExit').val('');
    $('#storage_amountCharged').val('');
    $('#storage_status').val('');
}

function getStorage() {
    var storage = {};

    $.get("http://localhost:8080/storage/get/"+$('#storage_ID').val(), function (data){
        //alert(JSON.stringify(data));
        storage = JSON.parse(data);
        //alert(storage.firstname);
        if(typeof(storage.error) !== undefined && storage.id != -1){
            $('#storage_ID').val(storage.id);
            $('#storage_itemID').val(storage.itemID);
            $('#storage_shelfNumber').val(storage.shelfNumber);
            $('#storage_rackNumber').val(storage.rackNumber);
            $('#storage_dateOfEntry').val(storage.dateOfEntry);
            $('#storage_expectedDateOfExit').val(storage.expectedDateOfExit);
            $('#storage_dateOfExit').val(storage.dateOfExit);
            $('#storage_amountCharged').val(storage.amountCharged);
            $('#storage_status').val(storage.status);

        }else if(storage.id == -1) {
            alert("not found");
            clear();
        }else{
            alert(JSON.stringify(data));
        }
    });

}

function deleteStorage() {

    $.get("http://localhost:8080/storage/delete/"+$('#storage_ID').val(), function (data){
        if(data == true){
            alert("Deleted Successfully.");
            clear();
            getAllStorages();
        }else{
            alert("Could not delete.");
        }
        clear();
    });
}

function updateStorage() {
    var storage = {};
    storage.id = parseInt($('#storage_ID').val());
    storage.itemID = $('#storage_itemID').val();
    storage.shelfNumber = $('#storage_shelfNumber').val();
    storage.rackNumber = $('#storage_rackNumber').val();
    storage.dateOfEntry = $('#storage_dateOfEntry').val();
    storage.expectedDateOfExit = $('#storage_expectedDateOfExit').val();
    storage.dateOfExit = $('#storage_dateOfExit').val();
    storage.amountCharged = $('#storage_shelfNumber').val();
    storage.status = $('#storage_status').val();

    alert(JSON.stringify(storage));

    $.ajax({
        url: "http://localhost:8080/storage/update",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(storage),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            //alert(response);
            if (response == true){
                alert("Successfully Updated");
                clear();
                getAllStorages();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
        }
    });
}
globalcustomers = [];
selectedCustomer = {};
function getAllCustomers(){
    $.ajax({
        url: "/customer/list",
        type: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                listCustomers(response);
            }
            setWrapperHeight();
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
            setWrapperHeight();
        }
    });
}

function listCustomers(customers){
    $('#customers').text('');
    globalcustomers = customers;
    for(customer in customers){
        node = "<tr>";
        node += "<td>"
        node += customers[customer].id;
        node += "</td>"
        node += "<td>"
        node += customers[customer].name;
        node += "</td>"
        node += "<td>"
        node += customers[customer].phoneNumber;
        node += "</td>"
        node += "<td>"
        node += customers[customer].email;
        node += "</td>"
        node += "<td>"
        node += customers[customer].dateOfRegistry;
        node += "</td>"
        node += "<td><span " +
            "onclick=\"loadCustomer("+customer+")\" " +
            ">load</span>"
        //onclick=\"loadCustomer(\""+JSON.stringify(customer)+"\");\"
        node += "</td>"
        node += "</tr>"
        $('#customers').append(node);
    }
}

function loadCustomer(customer){
    customerToLoad = globalcustomers[customer];
    clear();
    $('#customer_ID').val(customerToLoad.id);
    $('#customer_name').val(customerToLoad.name);
    $('#customer_phone').val(customerToLoad.phoneNumber);
    $('#customer_email').val(customerToLoad.email);
    $('#customer_date').val(customerToLoad.dateOfRegistry);
}

function addCustomer() {
    var customer = {};
    customer.id = $('#customer_ID').val();
    customer.name = $('#customer_name').val();
    customer.phoneNumber = $('#customer_phone').val();
    customer.email = $('#customer_email').val();
    customer.dateOfRegistry = $('#customer_date').val();
    alert(JSON.stringify(customer));
    $.ajax({
        url: "/customer/add",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(customer),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                alert(JSON.stringify(response));
                getAllCustomers();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
        }
    });
}

function clear(){
    $('#customer_ID').val('');
    $('#customer_name').val('');
    $('#customer_phone').val('');
    $('#customer_email').val('');
    $('#customer_date').val('');
}

function getCustomer() {
    var customer = {};

    $.get("/customer/get/"+$('#customer_ID').val(), function (data){
        //alert(JSON.stringify(data));
        customer = JSON.parse(data);
        //alert(customer.firstname);
        if(typeof(customer.error) !== undefined && customer.username != "not found"){
            $('#customer_ID').val(customer.id);
            $('#customer_name').val(customer.name);
            $('#customer_phone').val(customer.phoneNumber);
            $('#customer_email').val(customer.email);
            $('#customer_date').val(customer.dateOfRegistry);

        }else if(customer.username == "not found") {
            alert("not found");
            clear();
        }else{
            alert(JSON.stringify(data));
        }
    });

}

function deleteCustomer() {

    $.get("/customer/delete/"+$('#customer_ID').val(), function (data){
        if(data == true){
            alert("Deleted Successfully.");
            clear();
            getAllCustomers();
        }else{
            alert("Could not delete.");
        }
        clear();
    });
}

function updateCustomer() {
    var customer = {};
    customer.id = $('#customer_ID').val();
    customer.name = $('#customer_name').val();
    customer.phoneNumber = $('#customer_phone').val();
    customer.email = $('#customer_email').val();
    customer.dateOfRegistry = $('#customer_date').val();

    alert(JSON.stringify(customer));
    $.ajax({
        url: "/customer/update",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(customer),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            //alert(response);
            if (response == true){
                alert("Successfully Updated");
                clear();
                getAllCustomers();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
        }
    });
}

function itemDetails(){
    $('#item_view').show();
    $('#customer_view').hide();
    var customer = {};
    customer.id = $('#customer_ID').val();
    customer.name = $('#customer_name').val();
    customer.phoneNumber = $('#customer_phone').val();
    customer.email = $('#customer_email').val();
    customer.dateOfRegistry = $('#customer_date').val();
    selectedCustomer = customer;
    getAllItems();
}
package com.hashmiusama.warehouseapp.controllers;

import com.hashmiusama.warehouseapp.model.Item;
import com.hashmiusama.warehouseapp.service.ItemService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by seconduser on 11/23/16.
 */
@RestController
@RequestMapping("/item")
public class ItemController {
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Long itemAdd(@RequestBody Item item,
                              Model model) {
        System.err.println("INSERT ITEM CALLED!");
        System.err.println(item.toString());
        Long returnedID = ItemService.add(item);
        if(returnedID != -1L){
            System.out.println("ADDED!");
            return returnedID;
        }else{
            return -1L;
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public boolean itemUpdate(@RequestBody Item item,
                                  Model model) {
        System.err.println(item.toString());
        return ItemService.update(item);
    }

    @RequestMapping(value = "/delete/{ID}", method = RequestMethod.GET)
    public boolean itemDelete(@PathVariable Long ID, Model model) {
        return ItemService.delete(ID);
    }

    @RequestMapping(value = "/get/{ID}", method = RequestMethod.GET)
    public String itemGet(@PathVariable Long ID, Model model) {
        Item item = ItemService.get(ID);
        if (item != null) {
            return item.toString();
        }else {
            return "{\"error\":\"not found\"}";
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String itemList(Model model) {
        return ItemService.list();
    }
}

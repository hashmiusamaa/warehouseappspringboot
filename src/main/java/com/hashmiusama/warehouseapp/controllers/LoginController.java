package com.hashmiusama.warehouseapp.controllers;

import com.hashmiusama.warehouseapp.service.LoginService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by seconduser on 11/23/16.
 */

@RestController
public class LoginController {
    @RequestMapping(value = "/login/{username}/{password}", method = RequestMethod.GET)
    public String login(@PathVariable String username, @PathVariable String password, Model model) {
        return LoginService.login(username, password);
    }
}

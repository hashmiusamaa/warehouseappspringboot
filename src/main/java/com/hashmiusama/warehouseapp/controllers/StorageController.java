package com.hashmiusama.warehouseapp.controllers;

import com.hashmiusama.warehouseapp.model.Storage;
import com.hashmiusama.warehouseapp.service.StorageService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by seconduser on 11/23/16.
 */
@RestController
@RequestMapping("/storage")
public class StorageController {
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String storageAdd(@RequestBody Storage storage,
                          Model model) {
        System.err.println(storage.toString());
        if(StorageService.add(storage)){
            System.out.println("ADDED!");
            return storage.toString();
        }else{
            return "{\"error\":\"could not add\"}";
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public boolean storageUpdate(@RequestBody Storage storage,
                              Model model) {
        System.err.println(storage.toString());
        return StorageService.update(storage);
    }

    @RequestMapping(value = "/delete/{ID}", method = RequestMethod.GET)
    public boolean storageDelete(@PathVariable Long ID, Model model) {
        return StorageService.delete(ID);
    }

    @RequestMapping(value = "/get/{ID}", method = RequestMethod.GET)
    public String storageGet(@PathVariable Long ID, Model model) {
        Storage storage = StorageService.get(ID);
        if (storage != null) {
            return storage.toString();
        }else {
            return "{\"error\":\"not found\"}";
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String storageList(Model model) {
        return StorageService.list();
    }
}

package com.hashmiusama.warehouseapp.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by seconduser on 11/22/16.
 */
public class DBConnection {
    public static Connection connection;
    public static boolean initiateConnection(){
        boolean returnValue = false;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
            returnValue = true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            returnValue = false;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }
    public static boolean initializeDB(){
        boolean returnValue = false;
        if(initiateConnection()){
            try {
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);  // set timeout to 30 sec.
/*                statement.execute("drop table operatorinfo");
                statement.execute("drop table storageinfo");
                statement.execute("drop table iteminfo");
                statement.execute("drop table customerinfo");*/

                statement.execute(
                        "CREATE TABLE IF NOT EXISTS OperatorInfo(" +
                                "Firstname varchar(30) not null, " +
                                "Lastname varchar(30) not null, " +
                                "Username varchar(5) PRIMARY KEY, " +
                                "Password varchar(255) not null," +
                                "AdminRole bool default false);"
                );

                statement.execute(
                        "CREATE TABLE IF NOT EXISTS ItemInfo(" +
                                "ID INTEGER Primary key AUTO_INCREMENT, " +
                                "Name varchar(30) not null, " +
                                "CustomerID INTEGER not null, " +
                                "Weight FLOAT not null, " +
                                "DeclaredValue FLOAT not null DEFAULT 0, " +
                                "StorageID INTEGER not null, " +
                                "Operator varchar(5) not null," +
                                "Status INTEGER NOT NULL DEFAULT 0 )"
                );
                String sql = "insert into operatorinfo values(" +
                        "'"+ "Usama" +"'," +
                        "'"+ "Hashmi" +"'," +
                        "'"+ "minee" +"'," +
                        "'"+ "admin12" +"'," +
                        ""+ true +");";
                System.err.println(sql);
                statement.execute(sql);
                statement.execute(
                        "CREATE TABLE IF NOT EXISTS StorageInfo(" +
                                "ID INTEGER Primary key AUTO_INCREMENT, " +
                                "ItemID INTEGER, " +
                                "ShelfNumber INTEGER not null, " +
                                "RackNumber INTEGER not null, " +
                                "DateOfEntry DATE, " +
                                "ExpectedDateOfExit DATE, " +
                                "DateOfExit DATE, " +
                                "AmountCharged FLOAT DEFAULT 0," +
                                "Status INTEGER default 0 )"
                );
                statement.execute(
                        "CREATE TABLE IF NOT EXISTS CustomerInfo(" +
                                "ID INTEGER Primary key AUTO_INCREMENT, " +
                                "Name VARCHAR(20) not null, " +
                                "PhoneNumber VARCHAR(20) UNIQUE, " +
                                "Email VARCHAR(255) not null, " +
                                "DateOfRegistry DATE not null)"
                );
                returnValue = true;
            } catch (SQLException e) {
                e.printStackTrace();
                returnValue = false;
            }
            return returnValue;
        } else { return false; }
    }
}

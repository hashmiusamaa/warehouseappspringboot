package com.hashmiusama.warehouseapp.service;


import com.hashmiusama.warehouseapp.model.Customer;
import com.hashmiusama.warehouseapp.utils.DBConnection;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class CustomerService {
//insert into CUSTOMERINFO (name, phonenumber, email, dateofregistry) values('usama','48052463031','amajor@email.com','2016-10-10')
    public static boolean add(Customer customer){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "insert into customerinfo (name, phonenumber, email, dateofregistry) values(" +
                    "'"+ customer.getName() +"'," +
                    "'"+ customer.getPhoneNumber() +"'," +
                    "'"+ customer.getEmail() +"'," +
                    "'"+ customer.getDateOfRegistry() +"');";
            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        } finally{
            return returnValue;
        }

    }

    public static Customer get(Long ID){
        Statement statement = null;
        Date date = new Date(0);
        Customer customer = new Customer(-1L, "not found","not found","not found", date);
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select * from customerinfo where `ID` = "+ ID +";";
            System.err.println(sql);
            statement.execute(sql);
            ResultSet set = statement.getResultSet();

            while(set.next()){
                customer = new Customer(set.getLong(1), set.getString(2), set.getString(3), set.getString(4), set.getDate(5));
            }

            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return customer;
    }

    public static boolean update(Customer customer){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "update customerinfo set " +
                    "`name` = '"+customer.getName()+"'," +
                    "`phonenumber` = '"+customer.getPhoneNumber()+"'," +
                    "`email` = '"+customer.getEmail()+"'," +
                    "`dateofregistry` = '"+customer.getDateOfRegistry()+"' " +
                    "where `ID` = "+ customer.getId() +";";

            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }

    public static boolean delete(Long ID){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "delete from customerinfo where `ID` = "+ ID +";";
            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }

    public static String list(){
        String s = "[";
        Statement statement = null;
        Customer customer = null;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select * from customerinfo";
            statement.execute(sql);
            ResultSet set = statement.getResultSet();
            while(set.next()){
                customer = new Customer(
                        set.getLong(1),
                        set.getString(2),
                        set.getString(3),
                        set.getString(4),
                        set.getDate(5));
                s += customer.toString();
                s += ",";
            }
            s = s.substring(0, s.length() - 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        s += "]";
        return s;
    }
}

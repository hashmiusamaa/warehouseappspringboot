package com.hashmiusama.warehouseapp.service;

import com.hashmiusama.warehouseapp.model.Operator;
import com.hashmiusama.warehouseapp.utils.DBConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by seconduser on 11/23/16.
 */
public class LoginService {
    public static String login(String username, String password){
        Statement statement = null;
        boolean returnValue = false;
        boolean isAdminRole = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select adminRole from operatorinfo where `username` = '"+ username +"' and `password` = '"+password+"';";
            System.err.println(sql);
            statement.execute(sql);
            ResultSet set = statement.getResultSet();
            while(set.next()){
                isAdminRole = set.getBoolean(1);
                returnValue = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return "{\"exists\":"+returnValue+",\"adminRole\":"+isAdminRole+"}";
    }
}

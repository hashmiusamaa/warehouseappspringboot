package com.hashmiusama.warehouseapp.service;

import com.hashmiusama.warehouseapp.model.Storage;
import com.hashmiusama.warehouseapp.utils.DBConnection;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StorageService {
    //insert into storageinfo (itemid, shelfnumber, racknumber, dateofentry,expecteddateofexit, dateofexit, amountcharged,  status) values(1,1,1,'2016-1-1','2016-1-1','2016-1-1',1,'gone');
    public static boolean add(Storage storage){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "insert into storageinfo (itemid, shelfnumber, racknumber, dateofentry, " +
                    "expecteddateofexit, dateofexit, amountcharged,  status) values(" +
                    "'"+ storage.getItemID() +"'," +
                    "'"+ storage.getShelfNumber() +"'," +
                    "'"+ storage.getRackNumber() +"',";
            String dateOfEntry = null;
            String expectedDateOfExit = null;
            String dateOfExit = null;
            if (storage.getDateOfEntry() == null){
                dateOfEntry = ""+ storage.getDateOfEntry() +",";
            }else{
                dateOfEntry = "'"+ storage.getDateOfEntry() +"',";
            }
            if (storage.getExpectedDateOfExit() == null){
                expectedDateOfExit = ""+ storage.getExpectedDateOfExit() +"," ;
            }else{
                expectedDateOfExit = "'"+ storage.getExpectedDateOfExit() +"',";
            }
            if (storage.getDateOfExit() == null){
                dateOfExit = ""+ storage.getDateOfExit() +",";
            }else{
                dateOfExit = "'"+ storage.getDateOfExit() +"',";
            }
            sql +=  dateOfEntry +
                    expectedDateOfExit +
                    dateOfExit +
                    "'"+ storage.getAmountCharged() +"'," +
                    ""+ storage.getStatus() +");";
            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        } finally{
            return returnValue;
        }

    }

    public static Storage get(Long ID){
        Statement statement = null;
        Date date = new Date(1);
        Storage storage = new Storage(-1L, -1L, -1, -1, date, date, date, -1L, -1L);
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select * from storageinfo where `ID` = "+ ID +";";
            System.err.println(sql);
            statement.execute(sql);
            ResultSet set = statement.getResultSet();

            while(set.next()){
                storage = new Storage(set.getLong(1), set.getLong(2), set.getInt(3), set.getInt(4), set.getDate(5), set.getDate(6), set.getDate(7), set.getLong(8), set.getLong(9));
            }
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return storage;
    }

    public static boolean update(Storage storage){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String dateOfEntry = null;
            String expectedDateOfExit = null;
            String dateOfExit = null;
            if (storage.getDateOfEntry() == null){
                dateOfEntry = storage.getDateOfEntry()+", ";
            }else{
                dateOfEntry = "'"+ storage.getDateOfEntry() +"', ";
            }
            if (storage.getExpectedDateOfExit() == null){
                expectedDateOfExit = ""+ storage.getExpectedDateOfExit() +"," ;
            }else{
                expectedDateOfExit = "'"+ storage.getExpectedDateOfExit() +"',";
            }
            if (storage.getDateOfExit() == null){
                dateOfExit = ""+ storage.getDateOfExit() +",";
            }else{
                dateOfExit = "'"+ storage.getDateOfExit() +"',";
            }
            System.err.println(storage.toString());
            String sql = "update storageinfo set " +
                    "`itemid` = "+storage.getItemID()+"," +
                    "`shelfnumber` = "+storage.getShelfNumber()+"," +
                    "`racknumber` = "+storage.getRackNumber()+"," +
                    "`dateofentry` = "+ dateOfEntry +
                    "`expecteddateofexit` = "+ expectedDateOfExit +
                    "`dateofexit` = " + dateOfExit +
                    "`amountcharged` = "+storage.getAmountCharged()+", " +
                    "`status` = "+storage.getStatus()+" " +
                    "where `ID` = "+ storage.getId() +";";
            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }

    public static boolean delete(Long ID){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "delete from storageinfo where `ID` = "+ ID +";";
            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }

    public static String list(){
        String s = "[";
        Statement statement = null;
        Storage storage = null;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select * from storageinfo";
            statement.execute(sql);
            ResultSet set = statement.getResultSet();
            while(set.next()){
                storage = new Storage(set.getLong(1), set.getLong(2), set.getInt(3), set.getInt(4), set.getDate(5), set.getDate(6), set.getDate(7), set.getLong(8), set.getLong(9));
                s += storage.toString();
                s += ",";
            }
            s = s.substring(0, s.length() - 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        s += "]";
        return s;
    }
}

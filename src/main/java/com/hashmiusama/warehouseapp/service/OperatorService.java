package com.hashmiusama.warehouseapp.service;

import com.hashmiusama.warehouseapp.model.Operator;
import com.hashmiusama.warehouseapp.utils.DBConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class OperatorService {

    public static boolean add(Operator operator){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "insert into operatorinfo values(" +
                    "'"+ operator.getFirstName() +"'," +
                    "'"+ operator.getLastName() +"'," +
                    "'"+ operator.getUsername() +"'," +
                    "'"+ operator.getPassword() +"'," +
                    ""+ operator.isAdminRole() +");";
            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        } finally{
            return returnValue;
        }

    }

    public static Operator get(String username){
        Statement statement = null;
        Operator operator = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select * from operatorinfo where `username` = '"+ username +"';";
            System.err.println(sql);
            statement.execute(sql);
            ResultSet set = statement.getResultSet();

            while(set.next()){
  /*              System.err.println("zeroth column of set: "+set.getString(1));
                System.err.println("first column of set: "+set.getString(2));
                System.err.println("second column of set: "+set.getString(3));
                System.err.println("third column of set: "+set.getString(4));*/
                operator = new Operator(set.getString(1), set.getString(2), set.getString(3), set.getString(4), set.getBoolean(5));
            }

            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return operator;
    }

    public static boolean update(Operator operator){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "update operatorinfo set " +
                    "`firstname` = '"+operator.getFirstName()+"'," +
                    "`lastname` = '"+operator.getLastName()+"'," +
                    "`password` = '"+operator.getPassword()+"'," +
                    "`adminRole` = "+operator.isAdminRole()+" " +
                    "where `username` = '"+ operator.getUsername() +"';";

            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }

    public static boolean delete(String username){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "delete from operatorinfo where `username` = '"+ username +"';";
            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }

    public static String list(){
        String s = "[";
        Statement statement = null;
        Operator operator = null;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select * from operatorinfo where `username` != ''";
            statement.execute(sql);
            ResultSet set = statement.getResultSet();
            while(set.next()){
/*                System.err.println("zeroth column of set: "+set.getString(1));
                System.err.println("first column of set: "+set.getString(2));
                System.err.println("second column of set: "+set.getString(3));
                System.err.println("third column of set: "+set.getString(4));*/
                operator = new Operator(set.getString(1), set.getString(2), set.getString(3), set.getString(4), set.getBoolean(5));
                s += operator.toString();
                s += ",";
            }
            s = s.substring(0, s.length() - 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        s += "]";
        return s;
    }
}

package com.hashmiusama.warehouseapp.service;

import com.hashmiusama.warehouseapp.model.Item;
import com.hashmiusama.warehouseapp.utils.DBConnection;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ItemService {
// SELECT * FROM ITEMINFO
// insert into iteminfo (name, customerid, weight, declaredvalue, storageid, operator, status) values('mera item',10,10,10,10,'usama',0);

    public static Long add(Item item){
        Statement statement = null;
        boolean returnValue = false;
        Long id = -1L;
        try {
            System.out.println("STARTED");
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "insert into iteminfo (name, customerid, weight, declaredvalue, storageid, operator, status) values(" +
                    "'"+ item.getName() +"'," +
                    ""+ item.getCustomerID() +"," +
                    ""+ item.getWeight() +"," +
                    ""+ item.getDeclaredValue() +"," +
                    ""+ item.getStorageID() +"," +
                    "'"+ item.getOperator() +"'," +
                    ""+ item.getStatus()+");";
            System.err.println(sql);
            statement.execute(sql);
            sql = "select id from iteminfo order by 1 desc limit 1";
            statement.execute(sql);
            ResultSet set = statement.getResultSet();
            System.err.println(sql);
            while(set.next()){
                id = set.getLong(1);
            }
            returnValue = true;
            System.out.println("RECEIVED ID:" + id);
            System.out.println("DONE!");
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        } finally{
            return id;
        }

    }

    public static Item get(Long ID){
        Statement statement = null;
        Item item = new Item(-1L, "not found",0L,0L,0L, 0L, "not found", 0L);
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select * from iteminfo where `ID` = "+ID+";";
            System.err.println(sql);
            statement.execute(sql);
            ResultSet set = statement.getResultSet();

            while(set.next()){
                item = new Item(set.getLong(1), set.getString(2), set.getLong(3), set.getLong(4), set.getLong(5), set.getLong(6), set.getString(7), set.getLong(8));
            }

            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return item;
    }

    public static boolean update(Item item){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "update iteminfo set " +
                    "`name` = '"+item.getName()+"'," +
                    "`customerid` = "+item.getCustomerID()+"," +
                    "`weight` = '"+item.getWeight()+"'," +
                    "`declaredvalue` = '"+item.getDeclaredValue()+"'," +
                    "`storageid` = '"+item.getStorageID()+"'," +
                    "`operator` = '"+item.getOperator()+"'," +
                    "`status` = '"+item.getStatus()+"' " +
                    "where `id` = "+ item.getId() +";";            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }

    public static boolean delete(Long ID){
        Statement statement = null;
        boolean returnValue = false;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "delete from iteminfo where `ID` = "+ ID +";";
            System.err.println(sql);
            statement.execute(sql);
            returnValue = true;
        } catch (SQLException e) {
            e.printStackTrace();
            returnValue = false;
        }
        return returnValue;
    }

    public static String list(){
        String s = "[";
        Statement statement = null;
        Item item = null;
        try {
            statement = DBConnection.connection.createStatement();
            statement.setQueryTimeout(30);
            String sql = "select * from iteminfo";
            statement.execute(sql);
            ResultSet set = statement.getResultSet();
            while(set.next()){
                item = new Item(set.getLong(1), set.getString(2), set.getLong(3), set.getLong(4), set.getLong(5), set.getLong(6), set.getString(7), set.getLong(8));
                s += item.toString();
                s += ",";
            }
            s = s.substring(0, s.length() - 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        s += "]";
        return s;
    }
}

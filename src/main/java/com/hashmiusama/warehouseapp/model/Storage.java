package com.hashmiusama.warehouseapp.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Date;

/**
 * Created by seconduser on 11/22/16.
 */
public class Storage {
    private Long id;
    private Long itemID;
    private Integer shelfNumber;
    private Integer rackNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-dd-mm", locale = "pt-BR", timezone = "Brazil/East")
    private Date dateOfEntry;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-dd-mm", locale = "pt-BR", timezone = "Brazil/East")
    private Date expectedDateOfExit;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-dd-mm", locale = "pt-BR", timezone = "Brazil/East")
    private Date dateOfExit;
    private Long amountCharged;
    private Long status;

    public Long getId() {
        return id;
    }

    public void setId(Long ID) {
        this.id = ID;
    }

    public Long getItemID() {
        return itemID;
    }

    public void setItemID(Long itemID) {
        this.itemID = itemID;
    }

    public Integer getShelfNumber() {
        return shelfNumber;
    }

    public void setShelfNumber(Integer shelfNumber) {
        this.shelfNumber = shelfNumber;
    }

    public Integer getRackNumber() {
        return rackNumber;
    }

    public void setRackNumber(Integer rackNumber) {
        this.rackNumber = rackNumber;
    }

    public Date getDateOfEntry() {
        return dateOfEntry;
    }

    public void setDateOfEntry(Date dateOfEntry) {
        this.dateOfEntry = dateOfEntry;
    }

    public Date getExpectedDateOfExit() {
        return expectedDateOfExit;
    }

    public void setExpectedDateOfExit(Date expectedDateOfExit) {
        this.expectedDateOfExit = expectedDateOfExit;
    }

    public Date getDateOfExit() {
        return dateOfExit;
    }

    public void setDateOfExit(Date dateOfExit) {
        this.dateOfExit = dateOfExit;
    }

    public Long getAmountCharged() {
        return amountCharged;
    }

    public void setAmountCharged(Long amountCharged) {
        this.amountCharged = amountCharged;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"itemID\":" + itemID +
                ", \"shelfNumber\":" + shelfNumber +
                ", \"rackNumber\":" + rackNumber +
                ", \"dateOfEntry\":\"" + dateOfEntry + "\"" +
                ", \"expectedDateOfExit\":\"" + expectedDateOfExit + "\"" +
                ", \"dateOfExit\":\"" + dateOfExit + "\"" +
                ", \"amountCharged\":" + amountCharged +
                ", \"status\":" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Storage storage = (Storage) o;

        if (!id.equals(storage.id)) return false;
        return (rackNumber.equals(storage.rackNumber) && shelfNumber.equals(storage.shelfNumber));
    }

    public Storage(Long ID, Long itemID, Integer shelfNumber, Integer rackNumber, Date dateOfEntry, Date expectedDateOfExit, Date dateOfExit, Long amountCharged, Long status) {
        this.id = ID;
        this.itemID = itemID;
        this.shelfNumber = shelfNumber;
        this.rackNumber = rackNumber;
        this.dateOfEntry = dateOfEntry;
        this.expectedDateOfExit = expectedDateOfExit;
        this.dateOfExit = dateOfExit;
        this.amountCharged = amountCharged;
        this.status = status;
    }

    public Storage(){

    }
}

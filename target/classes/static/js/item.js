globalitems = [];
selectedItem = {};
function getAllItems(){
    $.ajax({
        url: "/item/list",
        type: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                listItems(response);
            }
            setWrapperHeight();
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
            setWrapperHeight();
        }
    });
}

function listItems(items){
    $('#items').text('');
    globalitems = items;
    for(item in items){
        node = "<tr>";
        node += "<td>"
        node += items[item].id;
        node += "</td>"
        node += "<td>"
        node += items[item].name;
        node += "</td>"
        node += "<td>"
        node += items[item].customerID;
        node += "</td>"
        node += "<td>"
        node += items[item].weight;
        node += "</td>"
        node += "<td>"
        node += items[item].declaredValue;
        node += "</td>"
        node += "<td>"
        node += items[item].storageID;
        node += "</td>"
        node += "<td>"
        node += items[item].operator;
        node += "</td>"
        node += "<td>"
        node += items[item].status;
        node += "</td>"
        node += "<td><span " +
            "onclick=\"loadItem("+item+")\" " +
            ">load</span>"
        //onclick=\"loadItem(\""+JSON.stringify(item)+"\");\"
        node += "</td>"
        node += "</tr>"
        $('#items').append(node);
    }
}

function loadItem(item){
    itemToLoad = globalitems[item];
    clear();
    $('#item_ID').val(itemToLoad.id);
    $('#item_name').val(itemToLoad.name);
    $('#item_customerID').val(itemToLoad.customerID);
    $('#item_weight').val(itemToLoad.weight);
    $('#item_declaredValue').val(itemToLoad.declaredValue);
    $('#item_storageID').val(itemToLoad.storageID);
    $('#item_operator').val(itemToLoad.operator);
    $('#item_status').val(itemToLoad.status);
}

function addItem() {
    var item = {};
    item.id = $('#item_ID').val();
    item.name = $('#item_name').val();
    item.customerID = selectedCustomer.id;
    item.weight = $('#item_weight').val();
    item.declaredValue = $('#item_declaredValue').val();
    item.storageID = $('#item_storageID').val();
    item.operator = localStorage['login'];
    item.status = $('#item_status').val();
    selectedItem = item;
    $.ajax({
        url: "/item/add",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(item),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            alert(response);
            if(response == -1){
                alert("Could not add item.");
            }else{
                alert(JSON.stringify(response));
                selectedItem.id = response;
                getAllItems();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
        }
    });
}

function clear(){
    $('#item_ID').val('');
    $('#item_name').val('');
    $('#item_customerID').val('');
    $('#item_weight').val('');
    $('#item_declaredValue').val('');
    $('#item_storageID').val('');
    $('#item_operator').val('');
    $('#item_status').val('');
}

function getItem() {
    var item = {};

    $.get("/item/get/"+$('#item_ID').val(), function (data){
        //alert(JSON.stringify(data));
        item = JSON.parse(data);
        //alert(item.firstname);
        if(typeof(item.error) !== undefined && item.id != -1){
            $('#item_ID').val(item.id);
            $('#item_name').val(item.name);
            $('#item_customerID').val(item.customerID);
            $('#item_weight').val(item.weight);
            $('#item_declaredValue').val(item.declaredValue);
            $('#item_storageID').val(item.storageID);
            $('#item_operator').val(item.operator);
            $('#item_status').val(item.status);

        }else if(item.id == -1) {
            alert("not found");
            clear();
        }else{
            alert(JSON.stringify(data));
        }
    });

}

function deleteItem() {

    $.get("/item/delete/"+$('#item_ID').val(), function (data){
        if(data == true){
            alert("Deleted Successfully.");
            clear();
            getAllItems();
        }else{
            alert("Could not delete.");
        }
        clear();
    });
}

function updateItem() {
    var item = {};
    item.id = $('#item_ID').val();
    item.name = $('#item_name').val();
    item.customerID = $('#item_customerID').val();
    item.weight = $('#item_weight').val();
    item.declaredValue = $('#item_declaredValue').val();
    item.storageID = $('#item_storageID').val();
    item.operator = localStorage['login'];
    item.status = $('#item_status').val();


    $.ajax({
        url: "/item/update",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(item),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            //alert(response);
            if (response == true){
                alert("Successfully Updated");
                clear();
                getAllItems();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
            setWrapperHeight();
        }
    });
}

function saveItem(){
    var item = {};
    item.id = $('#item_ID').val();
    item.name = $('#item_name').val();
    item.customerID = selectedCustomer.id;
    item.weight = $('#item_weight').val();
    item.declaredValue = $('#item_declaredValue').val();
    item.storageID = $('#item_storageID').val();
    item.operator = localStorage['login'];
    item.status = $('#item_status').val();
    selectedItem = item;
    alert(JSON.stringify(selectedItem));
    $("#item_view").hide();
    $("#storage_view").show();
    getAllStorages();
}
globaloperators = [];
function getAllOperators(){
    $.ajax({
        url: "/operator/list",
        type: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                listOperators(response);
            }
            setWrapperHeight();
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
        }
    });
}

function listOperators(operators){
    $('#operators').text('');
    globaloperators = operators;
    for(operator in operators){
        node = "<tr>";
        node += "<td>"
        node += operators[operator].username;
        node += "</td>"
        node += "<td>"
        node += operators[operator].password;
        node += "</td>"
        node += "<td>"
        node += operators[operator].firstname;
        node += "</td>"
        node += "<td>"
        node += operators[operator].lastname;
        node += "</td>"
        node += "<td>"
        node += "<input type=\"checkbox\" ";
        node += (operators[operator].adminRole)?"checked=\"checked\"":"";
        node += "disabled=\"disabled\" />";
        node += "</td>"
        node += "<td><span " +
            "onclick=\"loadOperator("+operator+")\" " +
            ">load</span>"
        //onclick=\"loadOperator(\""+JSON.stringify(operator)+"\");\"
        node += "</td>"
        node += "</tr>"
        $('#operators').append(node);
    }
}

function loadOperator(operator){
   operatorToLoad = globaloperators[operator];
    clear();
    $('#operator_firstname').val(operatorToLoad.firstname);
    $('#operator_lastname').val(operatorToLoad.lastname);
    $('#operator_username').val(operatorToLoad.username);
    $('#operator_password').val(operatorToLoad.password);
    $('#operator_adminrole').prop('checked', operatorToLoad.adminRole);
}

function addOperator() {
    var operator = {};
    operator.firstName = $('#operator_firstname').val();
    operator.lastName = $('#operator_lastname').val();
    operator.username = $('#operator_username').val();
    operator.password = $('#operator_password').val();
    operator.adminRole = $('#operator_adminrole').is(':checked');

    $.ajax({
        url: "/operator/add",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(operator),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            if(response.hasOwnProperty("error")){
                alert(response.error);
            }else{
                alert(JSON.stringify(response));
                getAllOperators();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
            clear();
        }
    });
}

function clear(){
    $('#operator_firstname').val('');
    $('#operator_lastname').val('');
    $('#operator_username').val('');
    $('#operator_password').val('');
    $('#operator_adminrole').checked = false;
}

function getOperator() {
    var operator = {};

    $.get("/operator/get/"+$('#operator_username').val(), function (data){
        //alert(JSON.stringify(data));
        operator = JSON.parse(data);
        //alert(operator.firstname);
        if(typeof(operator.error) !== undefined && operator.username != "not found"){
            $('#operator_firstname').val(operator.firstname);
            $('#operator_lastname').val(operator.lastname);
            $('#operator_username').val(operator.username);
            $('#operator_password').val(operator.password);
            $('#operator_adminrole').prop('checked', operator.adminRole);

        }else if(operator.username == "not found") {
            alert("not found");
            clear();
        }else{
            alert(JSON.stringify(data));
        }
    });

}

function deleteOperator() {

    $.get("/operator/delete/"+$('#operator_username').val(), function (data){
        if(data == true){
            alert("Deleted Successfully.");
            clear();
            getAllOperators();
        }else{
            alert("Could not delete.");
        }
        clear();
    });
}

function updateOperator() {
    var operator = {};
    operator.firstName = $('#operator_firstname').val();
    operator.lastName = $('#operator_lastname').val();
    operator.username = $('#operator_username').val();
    operator.password = $('#operator_password').val();
    operator.adminRole = $('#operator_adminrole').is(':checked');

    $.ajax({
        url: "/operator/update",
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        crossDomain: true,
        data: JSON.stringify(operator),
        contentType: 'application/json',
        dataType: "json",
        cache: false,
        processData: false,
        success: function (response) {
            //alert(response);
            if (response == true){
                alert("Successfully Updated");
                clear();
                getAllOperators();
            }
        },
        error: function (xhr, status) {
            alert(xhr);
        }
    });
}
function isLoggedIn(){
    if (localStorage['login'] == "false" || localStorage['login'] == undefined){
        return false;
    } else{
        return true;
    }
}

function isAdmin(){
    if(!isLoggedIn()){
        return false;
    }else{
        return localStorage['admin'];
    }
}

function login() {
    $.get("/login/"+$('#username').val()+"/"+$('#password').val(), function (data){
        returnVal = JSON.parse(data);
        if (returnVal.exists){
            localStorage['login'] = $('#username').val();
            localStorage['admin'] = returnVal.adminRole;
            alert("Logging in...");
            window.location.href = "/index.html";
        }else{
            alert("Wrong username or password.");
        }
    });
}

function logout(){
    localStorage['login'] = false;
    localStorage['admin'] = false;
    window.location.href = "/login.html";
}
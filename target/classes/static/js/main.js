

if (!isLoggedIn()){
    window.location.href = "/login.html";
}

function setWrapperHeight(){
    var totalHeight = 0;
    $(".wrapper").children(':visible').each(function(){
        totalHeight = totalHeight + $(this).outerHeight(true);
    });

    $(".wrapper").height(totalHeight);
}

$(document).ready(function(){
    if(isAdmin() != "false"){
        $('#operator_view').load('operator.html');

    }else{
        $('#operatorChange').hide();
    }
    $('#booking_view').load('booking.html');
    $('table thead tr td:text').wrapInner("<b></b>");
    setWrapperHeight();



});

function startProcess(){
    $('#booking_view').show();
    $('#customer_view').show();
    $('#customer_view').load('customer.html');
    getAllCustomers();
    $('#item_view').load('item_booking.html');
    $('#storage_view').load('storage_booking.html');
    setWrapperHeight();
}
Ant can be used to build the project by running in the project directory (where build.xml file is present), the command:
�ant -f ./build.xml�
Note that if you are in the directory, you will just have to give path as �.� as shown above.

The the software can be executed using command prompt/terminal using the command being in the Project Directory where build.xml exists
�java -cp .;./target/classes;./lib/* com.hashmiusama.warehouseapp� in windows OS and
�java -cp .:./target/classes:./lib/* com.hashmiusama.warehouseapp� in linux and mac

For further details read, user manual in the docs folder.